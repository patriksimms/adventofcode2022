import Foundation

func day1() throws -> Void {
    print("Task1 result: \(try task1())")
    print("Task2 result: \(try task2())")
}

func task1() throws -> Int {
    let filename = "input/day1.txt"

    let contents: String = try String(contentsOfFile: filename)

    let linesByElf = contents.components(separatedBy: "\n\n")

    let caloriesByElf = linesByElf.map {
        let meals = $0.components(separatedBy: "\n").map {
            (line) -> Int in
            return Int(line) ?? 0
        }

        return meals.reduce(0, +)
    }
    return caloriesByElf.max() ?? 0
}

func task2() throws -> Int {
   let filename = "input/day1.txt"

    let contents: String = try String(contentsOfFile: filename)

    let linesByElf = contents.components(separatedBy: "\n\n")

    var caloriesByElf = linesByElf.map {
        let meals = $0.components(separatedBy: "\n").map {
            (line) -> Int in
            return Int(line) ?? 0
        }

        return meals.reduce(0, +)
    }
    caloriesByElf.sort()
    caloriesByElf.reverse()
    return caloriesByElf[...2].reduce(0, +)
}

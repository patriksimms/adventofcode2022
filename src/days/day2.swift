struct Day2 {

    func run() throws {
        print("Task1 result: \(try task1())")
        print("Task2 result: \(try task2())")
    }

    func task1() throws -> Int {

        let filename = "input/day2.txt"
        var contents: String = try String(contentsOfFile: filename)
        var score = 0

        contents = contents.replacingOccurrences(of: "A", with: "X")  // Rock
        contents = contents.replacingOccurrences(of: "B", with: "Y")  // Paper
        contents = contents.replacingOccurrences(of: "C", with: "Z")  // Scissor
        var games = contents.components(separatedBy: "\n")
        games.removeLast()  // remove empty line

        for game in games {
            let enemyPlay = game.components(separatedBy: " ")[0]
            let playerPlay = game.components(separatedBy: " ")[1]

            if playerPlay == "X" {
                score += 1
            }
            else if playerPlay == "Y" {
                score += 2
            }
            else if playerPlay == "Z" {
                score += 3
            }

            if playerPlay == enemyPlay {
                score += 3  //draw
            }
            else if playerPlay == "X" && enemyPlay == "Z" {
                score += 6  // win
            }
            else if playerPlay == "Y" && enemyPlay == "X" {
                score += 6  // win
            }
            else if playerPlay == "Z" && enemyPlay == "Y" {
                score += 6  // win
            }
            else {
                // player has to loose, so we can automatically set +1
                score += 0
            }

        }
        return score
    }

    func task2() throws -> Int {
        let filename = "input/day2.txt"
        let contents: String = try String(contentsOfFile: filename)
        var score = 0

        var games = contents.components(separatedBy: "\n")
        games.removeLast()  // remove empty line

        for game in games {
            let enemyPlay = game.components(separatedBy: " ")[0]
            let neededPlayerMove = game.components(separatedBy: " ")[1]  // x -> loose, y -> draw, z -> win

            if neededPlayerMove == "X" {  // loose
                score += 0  // score for loosing
                if enemyPlay == "A" {  // rock
                    score += 3
                }
                else if enemyPlay == "B" {  // paper
                    score += 1
                }
                else {  // scissor
                    score += 2
                }

            }
            else if neededPlayerMove == "Y" {  // draw
                score += 3  // score for draw
                if enemyPlay == "A" {  // rock
                    // play rock
                    score += 1
                }
                else if enemyPlay == "B" {  // paper
                    score += 2
                }
                else {  // scissor
                    score += 3
                }

            }
            else {  // win
                score += 6  // score for win
                if enemyPlay == "A" {  // rock
                    // play scissors
                    score += 2
                }
                else if enemyPlay == "B" {  // paper
                    score += 3
                }
                else {  // scissor
                    score += 1
                }

            }

        }
        return score
    }
}

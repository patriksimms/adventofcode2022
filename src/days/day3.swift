extension Array {
    func split() -> (left: [Element], right: [Element]) {
        let ct = self.count
        let half = ct / 2
        let leftSplit = self[0..<half]
        let rightSplit = self[half..<ct]
        return (left: Array(leftSplit), right: Array(rightSplit))
    }
}

struct Backpack {
    private var firstCompartment: [Character]
    private var secondCompartment: [Character]
    private var items: [Character]

    init(inventory: String) {
        let compartments = Array(inventory).split()
        items = Array(inventory)
        firstCompartment = compartments.left
        secondCompartment = compartments.right
    }

    func findDuplicateItem() -> Character {
        for item in firstCompartment {
            if secondCompartment.contains(item) {
                return item
            }
        }
        return " "
    }

    func intersections(firstOhter: Backpack, secondOther: Backpack) -> Set<Character> {
        return Set(self.items).intersection(Set(firstOhter.items)).intersection(
            Set(secondOther.items)
        )
    }
}

struct Day3 {

    func run() throws {
        print("Task1 result: \(try task1())")
        print("Task2 result: \(try task2())")
    }

    func getWeight(char: Character) -> Int {
        return Int(char.asciiValue!) - Int(char.isUppercase ? 38 : 96)
    }

    func task1() throws -> Int {
        let filename = "input/day3.txt"
        var contents: String = try String(contentsOfFile: filename)
        contents.removeLast()  // remove empty line

        let backpacks = contents.components(separatedBy: "\n").map { Backpack(inventory: $0) }

        let duplicates = backpacks.map { $0.findDuplicateItem() }
        let weights =
            duplicates.map { getWeight(char: $0) }

        return weights.reduce(0, +)
    }

    func task2() throws -> Int {
        let filename = "input/day3.txt"
        var contents: String = try String(contentsOfFile: filename)
        contents.removeLast()  // remove empty line
        var weights = 0

        let backpacks = contents.components(separatedBy: "\n").map { Backpack(inventory: $0) }

        for i in stride(from: 0, to: backpacks.count, by: 3) {
            weights +=
                getWeight(
                    char: backpacks[i].intersections(
                        firstOhter: backpacks[i + 1],
                        secondOther: backpacks[i + 2]
                    ).first ?? " "
                )
        }
        return weights
    }
}

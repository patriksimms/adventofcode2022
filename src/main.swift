import Foundation
import Guaka

extension String {
    var isNumber: Bool {
        let digitsCharacters = CharacterSet(charactersIn: "0123456789")
        return CharacterSet(charactersIn: self).isSubset(of: digitsCharacters)
    }
}

var result = ""

let runCommand = Command(usage: "Hello") {
    _,
    args in
    print("Executing day \(args[0])...")

    guard args.count == 1 else {
        runCommand.fail(statusCode: 1, errorMessage: "Too many arguments provided!")
    }

    guard let day = Int(args[0]) else {
        runCommand.fail(statusCode: 1, errorMessage: "argument[0] \"\(args[0])\" is not a number!")
    }

    do {
        switch day {
        case 1:
            try day1()
        case 2:
            try Day2().run()
        case 3:
            try Day3().run()
        default:
            runCommand.fail(statusCode: 1, errorMessage: "Invalid day \"\(args[0])\"")
        }
    }
    catch {
        print(error)
        runCommand.fail(statusCode: 1, errorMessage: "Error during day execution!")
    }

    print(result)
}

runCommand.execute()

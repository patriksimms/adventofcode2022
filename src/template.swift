struct DayX {

    func run() throws {
        print("Task1 result: \(try task1())")
        print("Task2 result: \(try task2())")
    }

    func task1() throws -> Int {
        return 0
    }

    func task2() throws -> Int {
        return 0
    }
}

// swift-tools-version: 5.9

import PackageDescription

let package = Package(
  name: "AoC2022",
  dependencies: [
    .package(url: "https://github.com/nsomar/Guaka", from: "0.4.1")
  ],
  targets: [
    .executableTarget(
      name: "runDay",
      dependencies: ["Guaka"]
    )
  ]
)
